package GCPlugins::GCbooks::GCBdphile;

###################################################
#
#  Copyright 2005-2007 Jonas
#
#  This file is part of GCstar.
#
#  GCstar is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  GCstar is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with GCstar; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
#
###################################################

use strict;
use utf8;

use GCPlugins::GCbooks::GCbooksCommon;

{
    # Replace SiteTemplate with your exporter name
    # It must be the same name as the one used for file and main package name
    package GCPlugins::GCbooks::GCPluginBdphile;

    use base 'GCPlugins::GCbooks::GCbooksPluginsBase';

    # getSearchUrl
    # Used to get the URL that to be used to perform searches.
    # $word is the query
    # Returns the full URL.
    sub getSearchUrl
    {
        my ($self, $word) = @_;
        my $url;

        $url = 'https://www.bdphile.info/search/album/?q='.$word;

        return $url;
    }

    # getItemUrl
    # Used to get the full URL of an item page.
    # Useful when url on results pages are relative.
    # $url is the URL as found with a search.
    # Returns the absolute URL.
    sub getItemUrl
    {
        my ($self, $url) = @_;

        return $url;
    }

    # getCharset
    # Used to convert charset in web pages.
    # Returns the charset as specified in pages.
    sub getCharset
    {
        my $self = shift;

        return "ISO-8859-1";
    }

    # getName
    # Used to display plugin name in GUI.
    # Returns the plugin name.
    sub getName
    {
        return "Bdphile";
    }

    # getAuthor
    # Used to display the plugin author in GUI.
    # Returns the plugin author name.
    sub getAuthor
    {
        return 'Jonas';
    }

    # getLang
    # Used to fill in plugin list with user language plugins
    # Return the language used for this site (2 letters code).
    sub getLang
    {
        return 'FR';
    }

    sub getSearchFieldsArray
    {
        return ['isbn','title'];
    }
    # getExtra
    # Used if the plugin wants an extra column to be displayed in search results
    # Return the column title or empty string to hide the column.
    sub getExtra
    {
        return 'Type d\'édition';
    }

    # getNumberPasses
    # Used to set the number of search "passes" the plugin requires. This defaults to
    # a single pass, but for some sites 2 or more searches are required. See the GCTvdb
    # plugin for an example of such a site
    sub getNumberPasses
    {
        return 1;
    }

    # changeUrl
    # Can be used to change URL if item URL and the one used to
    # extract information are different.
    # Return the modified URL.
    sub changeUrl
    {
        my ($self, $url) = @_;

        return $url;
    }

    # In processing functions below, self->{parsingList} can be used.
    # If true, we are processing a search results page
    # If false, we are processing a item information page.

    # $self->{inside}->{tagname} (with correct value for tagname) can be used to test
    # if we are in the corresponding tag.

    # You have a counter $self->{itemIdx} that have to be used when processing search results.
    # It is your responsability to increment it!

    # When processing search results, you have to fill the available fields for results
    #
    #  $self->{itemsList}[$self->{movieIdx}]->{field_name}
    #
    # When processing a movie page, you need to fill the fields (if available)
    # in $self->{curInfo}.
    #
    #  $self->{curInfo}->{field_name}

    # start
    # Called each time a new HTML tag begins.
    # $tagname is the tag name.
    # $attr is reference to an associative array of tag attributes.
    # $attrseq is an array reference containing all the attributes name.
    # $origtext is the tag text as found in source file
    # Returns nothing
    sub start
    {
        my ($self, $tagname, $attr, $attrseq, $origtext) = @_;
        $self->{inside}->{$tagname}++;

        if ($self->{parsingList})
        {
            # Your code for processing search results here
            if (($tagname eq 'a') && (index($attr->{href},"album/view/") >= 0))
            {
                $self->{itemIdx}++ ;
                $self->{isTitle} = 1 ;
                $self->{isPublication} = 0 ;
                $self->{itemsList}[$self->{itemIdx}]->{url} = $attr->{href} ;
                #~ $self->{itemsList}[$self->{itemIdx}]->{title} = "" ;
                $self->{itemsList}[$self->{itemIdx}]->{cover} = "https://www.bdphile.info/static/images/media/cover/".substr($self->{itemsList}[$self->{itemIdx}]->{url},36,-1).".jpg" ;
            }
        }
        else
        {
            if (($tagname eq 'div') && ($attr->{id} eq "reeditions"))
            {
                $self->{isReeditions} = 1;
            }
            if ($self->{isReeditions} == 0)
                {
                if ($tagname eq 'h1')
                {
                    $self->{isSerie} = 1 ;
                }
                elsif ($tagname eq 'h2')
                {
                    $self->{isTitle} = 1 ;
                }
                elsif ($tagname eq 'dt')
                {
                    $self->{isFieldName} = 1;
                    $self->{fieldName} = "" ;
                }
                elsif ($tagname eq 'dd')
                {
                    $self->{isFieldValue} = 1;
                }
                elsif (($tagname eq 'p') && ($attr->{class} =~ /synopsis/))
                {
                    print "\nDBG: isDescription ";
                    $self->{isDescription} = 1;
                }
                elsif (($tagname eq 'div') && ($attr->{id} eq "book-picture"))
                {
                    $self->{isCover} = 1;
                }
                elsif (($tagname eq 'div') && ($attr->{id} eq "extraits"))
                {
                    $self->{isBackpic} = 1;
                }

                elsif ($self->{isCover} && ($tagname eq 'a'))
                {
                    $self->{curInfo}->{cover} = $attr->{href};
                    $self->{isCover} = 0 ;
                }
                elsif ($self->{isBackpic} && ($tagname eq 'a'))
                {
                    $self->{curInfo}->{backpic} = $attr->{href};
                    print "\nDBG: backpic ".$self->{curInfo}->{backpic};
                    $self->{isBackpic} = 0 ;
                }
            }
        }
    }

    # end
    # Called each time a HTML tag ends.
    # $tagname is the tag name.
    sub end
    {
        my ($self, $tagname) = @_;
        $self->{inside}->{$tagname}--;

        if ($self->{parsingList})
        {
            # Your code for processing search results here
            if (($self->{isTitle}) && ($tagname eq 'a'))
            {
                $self->{isTitle} = 0 ;
                $self->{isPublication} = 1 ;
            }
        }
        else
        {
            if ($self->{isReeditions} == 0)
            {
                if ($tagname eq 'a')
                {
                    $self->{isSerie} = 0
                }
                elsif ($tagname eq 'h2')
                {
                    $self->{isTitle} = 0
                }
                elsif ($tagname eq 'dt')
                {
                    $self->{isFieldName} = 0
                }
                elsif ($tagname eq 'dd')
                {
                    if ($self->{author} ne "")
                    {

                        no warnings 'experimental::smartmatch';
                        if (not $self->{author} ~~ @{$self->{curInfo}{authors}})
                        {
                            push @{$self->{curInfo}{authors}}, [$self->{author}];
                        }
                        # alternative to remove the use of the experimental smartmatch operator
                        #my $value = $self->{author};
                        #if (! grep( /^$value$/, @{$self->{curInfo}{authors}} ) )
                        #{
                        #    print "Did not find it";
                        #    push @{$self->{curInfo}{authors}}, [$self->{author}];
                        #}
                        $self->{author} = "";
                    }
                    $self->{isFieldValue} = 0
                }
                elsif ($tagname eq 'div')
                {
                    $self->{isDescription} = 0;
                }
            }
        }
    }

    # text
    # Called each time some plain text (between tags) is processed.
    # $origtext is the read text.
    sub text
    {
        my ($self, $origtext) = @_;

       if ($self->{parsingList})
        {
            # Your code for processing search results here
            if ($self->{isTitle})
            {
                if($origtext =~ /(.*)\s\((.*)\)\s(.*)/)
                {
                    $self->{itemsList}[$self->{itemIdx}]->{serie} = $1 ;
                    $self->{itemsList}[$self->{itemIdx}]->{extra} = $2 ;
                    $self->{itemsList}[$self->{itemIdx}]->{title} = $3 ;
                }
                else
                {
                    $self->{itemsList}[$self->{itemIdx}]->{title} = $origtext
                }
            }
            elsif ($self->{isPublication})
            {
                return if ($self->{itemIdx} < 0);
                $self->{itemsList}[$self->{itemIdx}]->{publication} = substr($origtext, 2);
            }
        }
        else
        {
            # Enleve les blancs en debut de chaine
            if ($self->{isReeditions} == 0)
            {
                $origtext =~ s/^\s+//;

                if ($self->{isSerie})
                {
                    $self->{curInfo}->{serie} .= $origtext;
                }
                elsif ($self->{isTitle})
                {
                    if ($self->{curInfo}->{serie} eq "")
                    {
                        $self->{curInfo}->{title} = $origtext;
                    }
                    else
                    {
                        $self->{curInfo}->{title} = $self->{curInfo}->{serie}.", ".$origtext; # met en prefix la série
                    }
                }
                elsif ($self->{isFieldName})
                {
                    $self->{fieldName} = $origtext;
                }
                elsif ($self->{isFieldValue})
                {
                    if (($self->{fieldName} eq "Scénario") || ($self->{fieldName} eq "Dessin") || ($self->{fieldName} eq "Couleurs") || ($self->{fieldName} eq "Lettrage"))
                    {
                        $self->{author} .= $origtext;
                    }
                    elsif ($self->{fieldName} =~  /diteur/)
                    {
                        $self->{curInfo}->{publisher} .= $origtext;
                    }
                    elsif ($self->{fieldName} eq "Date de publication")
                    {
                        $self->{curInfo}->{publication} .= $origtext;
                    }
                    elsif ($self->{fieldName} eq "Format")
                    {
                        $self->{curInfo}->{format} .= $origtext;
                    }
                    elsif ($self->{fieldName} eq "EAN")
                    {
                        $self->{curInfo}->{isbn} .= $origtext;
                    }
                }
                elsif ($self->{isDescription})
                {
                    if ($origtext =~ /Le synopsis de cet album est manquant/)
                    {
                        $self->{isDescription} = 0;
                    }
                    else
                    {
                        $self->{curInfo}->{description} .= $origtext;
                    }
                }
                $self->{curInfo}->{web} = $self->{itemsList}[$self->{wantedIdx}]->{url};

            }
        }
    }

    # new
    # Constructor.
    # Returns object reference.
    sub new
    {
        my $proto = shift;
        my $class = ref($proto) || $proto;
        my $self  = $class->SUPER::new();

        # This member should be initialized as a reference
        # to a hash. Each keys is a field that could be
        # in results with value 1 or 0 if it is returned
        # or not. For the list of keys, check the model file
        # (.gcm) and search for tags <field> in
        # /collection/options/fields/results
        $self->{hasField} = {
            serie => 1,
            title => 1,
            authors => 0,
            publisher => 0,
            publication => 1,
            format => 0,
            edition => 0,
            web => 0,
            description => 0,
            isbn => 0,
        };

        bless ($self, $class);

        $self->{isTitle} = 0;
        $self->{isReeditions} = 0;
        $self->{isPublication} = 0;
        $self->{author} = "";
        @{$self->{curInfo}{authors}} = ();

        return $self;
    }

    # getFields
    # Called on each pass, by plugins with getNumberPasses > 1
    # to get the columns return by the plugin during that pass
    #~ sub getReturnedFields
    #~ {
        #~ my $self = shift;

        #~ # This member should be initialized as a reference
        #~ # to a hash. Each keys is a field that could be
        #~ # in results with value 1 or 0 if it is returned
        #~ # or not. For the list of keys, check the model file
        #~ # (.gcm) and search for tags <field> in
        #~ # /collection/options/fields/results
        #~ $self->{hasField} = {
            #~ title => 1,
        #~ };
    #~ }

    # preProcess
    # Called before each page is processed. You can use it to do some substitutions.
    # $html is the page content.
    # Returns modified version of page content.
    sub preProcess
    {
        my ($self, $html) = @_;

        $self->{isTitle} = 0;
        $self->{isReeditions} = 0;
        $self->{isPublication} = 0;
        $self->{author} = "";

        $html =~ s|<br />|\n|gi;

        return $html;
    }

}

1;
