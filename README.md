README
=====================


Plugin de recherche GCstar pour les collections de livre (GCbooks) sur le
site BDphile.

Ce plugin est compatible avec le fork de Kerenoc https://gitlab.com/Kerenoc/GCstar
la compatibilitée avec le code d'origine de Tian n'est plus assurée.


INSTALLATION
---------------------

Ce plugin a été intégré par Kerenoc avec de légères variantes. Pour utiliser
cette version remplacez /gcstar/lib/gcstar/GCPlugins/GCbooks/GCBdphile.pm
par la version de ce repo.


SUPPORT
---------------------

http://forums.gcstar.org/viewtopic.php?id=18093


BUGS REPORT / FEATURES REQUEST
---------------------

https://gitlab.com/snouf/gcstar/issues or http://forums.gcstar.org/viewtopic.php?id=18093
